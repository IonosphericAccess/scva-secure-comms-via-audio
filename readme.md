# SCVA - Secure Comms Via Audio (SCVA_2.0.apk is available in Downloads)


Warning: SCVA is not meant to be used on your main phone, or any phone that has network access. This app is designed to work on an air-gapped device, <br>
so your keys are offline and data cannot be exfiltrated by a rootkit. <br>
Simply putting the device in airplane mode is weak security, a physically disabled device with the antennas removed and antenna pcb traces cut is better. Dip switches which <br>
disable the radios are the best option, like the pinephone running android.<br><br>
SCVA works on phone calls by holding your airgapped phone speaker and mic next to a networked phone speaker and mic, which is hosting the phone call,<br>
and it works good with both phones sitting next to one another on a desk. SCVA works Via Audio, like AOL did in the past, by running on a 56k modem <br>
over your phone landline.
<br><br>
This concept may seem bizarre, however the concept is no different from a hardware crypto wallet. <br>
In fact, although the message is sent via audio, the keys are shared by this app via QR code. You share an on-screen QR code, which is accepted by the camera on the second device.<br>
Crypto wallets are offline devices with no radios. The QR code is the only way in or out.<br>
If the concept of an offline computer that stores your crypto keys is a winning architecture, then the concept of an offline computer that stores your communication keys<br>
should not be a bridge too far. Someday everyone may walk around with a dedicated hardware crypto currency wallet, and a dedicated crypto comms device.

<br><br>
SCVA works over cell phones, land lines, radios, walkie talkies, intercoms, or any system designed to carry the human voice. You can even record an audio file and send that over a compromised network. <br>
SCVA is not meant for long messages. It is designed to send short important messages. Use SCVA for the most sensitive part of your conversation,<br>
where is the rendezvous, what flight you are on, location, addresses, bank account number, etc, just that one crucial line. Use traditional methods to communicate<br>
the rest of the conversation because bandwidth is limited.
<br><br>
Bandwidth is very limited, as the data is converted into sound, and back into data, but that is a security feature. It takes about 30 seconds to send a bitcoin private key,<br>
(which is only 256 characters) and therefore, installing a rootkit to exfiltrate data is impossible to do in a short period of time. However, even if the attacker had infinite time, <br>
the attack would be obvious to the legitimate user, as an attempt at this type of operation would lengthen the message into a untenable size, hours or days, and would be therefore <br>
be aborted by the user instantly. <br>
This is why we believe SCVA to be the most secure app in the world.

For beta testing please follow these instructions:

1. go to settings
2. check Use list of common modes
3. go to mode list -> check use BPSK31
4. go to settings-> modem -> Reed -Solomon FEC-> enable
5. settings -> encryption -> encryption key -> make a key -> activate key -> share with second device
6. enable encryption

note: if you have done this correctly you will see: SCVA - BPSK31 - Listening@1500Hz | Active key is XXXXX
<br>if mode is ever wrong, use PREV MODE, NEXT MODE to get back to BPSK31
<br>if Hz is ever off use +/- above S2N to adjust, found on the waterfall screen, or touch the red box on the waterfall

7. go to waterfall screen by swiping right twice, uncheck Rx RsID, uncheck Tx RsID, uncheck AFC
8. on RX device hit SQLCH DOWN twice
9. on tx device go to tx screen by swiping right once-> type message -> hit SEND TEXT