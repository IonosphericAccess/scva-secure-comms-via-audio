/*
 * Preferences.java  
 *   
 * Copyright (C) 2011 John Douyere (VK2ETA)  
 *   
 * This program is distributed in the hope that it will be useful,  
 * but WITHOUT ANY WARRANTY; without even the implied warranty of  
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.  
 *   
 * You should have received a copy of the GNU General Public License  
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.  
 */

package com.SCVA;

/**
 * @author John Douyere <vk2eta@gmail.com>
 */

import com.SCVA.KeyShare.KeysActivity;
import com.SCVA.KeyShare.Utils.Utils;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.widget.Toast;

public class myPreferences extends PreferenceActivity
{
	int key_lenght = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		/*
		 * No theme for the preferences as the checkboxes do not show on some devices.
		 * Use system default instead. //Set the Activity's Theme int myTheme =
		 * config.getPreferenceI("APPTHEME", 0); switch (myTheme) { case 1:
		 * setTheme(R.style.scvaStandardDark); break;
		 * 
		 * case 2: setTheme(R.style.scvaSmallScreen); break; default:
		 * setTheme(R.style.scvaStandard); break; }
		 */
		// Start from the fixed section of the preferences
		addPreferencesFromResource(R.xml.preferences);

		// Now add the dynamic part of the preferences (mode list etc...).
		PreferenceCategory targetCategory = (PreferenceCategory) findPreference("listofmodestouse");

		for (int i = 0; i < Modem.numModes; i++)
		{
			// create one check box for each setting you need
			CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
			// make sure each key is unique
			checkBoxPreference.setKey("USE" + Modem.modemCapListString[i]);
			checkBoxPreference.setTitle("Use " + Modem.modemCapListString[i]);
			// checkBoxPreference.setChecked(true);
			targetCategory.addPreference(checkBoxPreference);
		}

		Preference enc_key = (Preference) findPreference("enc_key");
		enc_key.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				Utils.println("=======================================");
				startActivity(new Intent(myPreferences.this, KeysActivity.class));
				return true;
			}
		});

		// Preference generateRandEncKey = findPreference("generate_random_key");

		// if (SCVA.mysp.getString("enc_algorithm", "").equals("blowfish")) key_lenght =
		// 56;
		// else
		// key_lenght = 32;
		// generateRandEncKey.setOnPreferenceClickListener(new
		// Preference.OnPreferenceClickListener()
		// {
		// @Override
		// public boolean onPreferenceClick(Preference preference)
		// {
		// Toast.makeText(myPreferences.this, "Random Encryption Key Generated",
		// Toast.LENGTH_SHORT).show();
		//
		// // EditTextPreference enc_key = (EditTextPreference)
		// findPreference("enc_key");
		//
		// RandomString rn = new RandomString(key_lenght);
		// String kk = rn.nextString();
		// SCVA.mysp.edit().putString("enc_key", kk).commit();
		// // enc_key.setText(kk);
		// return false;
		// }
		// });
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		// Use instance field for listener
		// It will not be gc'd as long as this instance is kept referenced

		SCVA.splistener = new SharedPreferences.OnSharedPreferenceChangeListener()
		{
			public void onSharedPreferenceChanged(SharedPreferences prefs, String key)
			{
				if (SCVA.mysp.getString("enc_algorithm", "").equals("blowfish")) key_lenght = 56;
				else
					key_lenght = 32;

				// Implementation
				if (key.equals("AFREQUENCY") || key.equals("SLOWCPU") || key.startsWith("USE") || key.equals("RSID_ERRORS") || key.equals("RSIDWIDESEARCH"))
				{
					SCVA.RXParamsChanged = true;
				}

				// Validate Encryption Key lenght
				if (SCVA.mysp.getBoolean("use_enc", false))
				{
					if (SCVA.mysp.getString("enc_key", "").length() != key_lenght)
					{
						// Toast.makeText(myPreferences.this, ("Invalid key.\nlenght must be " +
						// key_lenght + " for " + SCVA.mysp.getString("enc_algorithm", "") + "
						// algorithm"),
						// Toast.LENGTH_LONG).show();
						Toast.makeText(myPreferences.this, "Please add and Unlock a key to enable encryption", Toast.LENGTH_LONG).show();
						SCVA.mysp.edit().putBoolean("use_enc", false).commit();
						CheckBoxPreference enc_use = (CheckBoxPreference) findPreference("use_enc");
						enc_use.setChecked(false);
					}
				}
			}
		};
		SCVA.mysp.registerOnSharedPreferenceChangeListener(SCVA.splistener);
	}
}