package com.SCVA.KeyShare;

import java.util.ArrayList;

import com.SCVA.KeyShare.Utils.Utils;
import com.SCVA.R;
import com.SCVA.KeyShare.Utils.KeyStore;
import com.SCVA.KeyShare.adapters.KeyAdapter;
import com.SCVA.KeyShare.models.Key;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;


public class KeysActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener
{
	private ImageView addKey;
	private ListView listView;
	private KeyStore keyStore;
	private KeyAdapter adapter;
	private ArrayList<Key> keysList;
	public static SharedPreferences mysp;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_keys);

		mysp = PreferenceManager.getDefaultSharedPreferences(this);
		// mysp.edit().putString("enc_keys", "").commit();
		keysList = new ArrayList<>();

		keyStore = new KeyStore();
		keysList = keyStore.readKeys();

		addKey = findViewById(R.id.addKey);
		addKey.setOnClickListener(this);

		listView = findViewById(R.id.keysList);

		adapter = new KeyAdapter(this, R.layout.keys_list_item, keysList, listView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);

		runOnUiThread(populate);

		// Utils.println(new Utils().generateMd5("hello"));
		// Utils.println(new Utils().generateMd5("hello how are you hello how are
		// you hello how are you").length());

		// String ba = Base64.toBase64String("1234567812345678".getBytes());
		// Utils.println(ba);
		// Utils.println(Base64.decode(ba));
	}

	@Override
	public void onClick(View view)
	{
		if (view == addKey)
		{
			showDialog();
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		runOnUiThread(populate);
	}

	Runnable populate = new Runnable()
	{
		@Override
		public void run()
		{
			adapter.clear();
			keysList = keyStore.readKeys();
			for (int i = 0; i < keysList.size(); i++)
			{
				adapter.add(keysList.get(i));
			}
			adapter.notifyDataSetChanged();
		}
	};

	public void showDialog()
	{
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.new_key_dialog);

		ViewGroup rKey = (ViewGroup) dialog.findViewById(R.id.rKey);
		rKey.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				startActivity(new Intent(KeysActivity.this, AddKeyActivity.class));
			}
		});
		ViewGroup pKey = (ViewGroup) dialog.findViewById(R.id.pKey);
		pKey.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				Utils.println("Password Key");
				startActivity(new Intent(KeysActivity.this, AddPasswordBasedKeyActivity.class));
			}
		});
		ViewGroup iKey = (ViewGroup) dialog.findViewById(R.id.iKey);
		iKey.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Utils.println("Import Key");
				dialog.dismiss();
				startActivity(new Intent(KeysActivity.this, ImportKeyActivity.class));
			}
		});
		dialog.show();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		Intent intent = new Intent(KeysActivity.this, KeyInfoActivity.class);
		intent.putExtra("KEY", keysList.get(position));
		startActivity(intent);
	}
}
