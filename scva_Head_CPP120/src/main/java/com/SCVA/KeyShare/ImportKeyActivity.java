package com.SCVA.KeyShare;

import java.util.Date;

import com.SCVA.R;
import com.SCVA.KeyShare.Utils.KeyStore;
import com.SCVA.KeyShare.Utils.Utils;
import com.SCVA.KeyShare.models.Key;
import com.SCVA.encryption.Aes256Encryptor;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class ImportKeyActivity extends Activity implements View.OnClickListener, QRCodeReaderView.OnQRCodeReadListener
{
	private Button save;
	private EditText name;
	private KeyStore keyStore;
	private String keyText = "";
	private boolean found = false;
	private String plainKey = "";
	private Bitmap qrCodeImage = null;
	public static SharedPreferences mysp;
	private QRCodeReaderView qrCodeReaderView;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.activity_import_key);

		mysp = PreferenceManager.getDefaultSharedPreferences(this);

		name = findViewById(R.id.name);

		keyStore = new KeyStore();

		qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
		qrCodeReaderView.setOnQRCodeReadListener(this);

		// Use this function to enable/disable decoding
		qrCodeReaderView.setQRDecodingEnabled(true);

		// Use this function to change the autofocus interval (default is 5 secs)
		qrCodeReaderView.setAutofocusInterval(2000L);

		// Use this function to enable/disable Torch
		// qrCodeReaderView.setTorchEnabled(true);

		// Use this function to set front camera preview
		// qrCodeReaderView.setFrontCamera();

		// Use this function to set back camera preview
		qrCodeReaderView.setBackCamera();

		save = findViewById(R.id.save);
		save.setOnClickListener(this);
	}

	@Override
	public void onClick(View view)
	{
		if (view == save)
		{
			if (name.getText().toString().length() < 5)
			{
				Toast.makeText(ImportKeyActivity.this, "Please enter at least 5 characters", Toast.LENGTH_SHORT).show();
				return;
			}
			Key key = new Key();

			key.setName(name.getText().toString());
			key.setKeyText("" + keyText);
			key.setCreatedOn(new Date(System.currentTimeMillis()));
			key.setDefault(false);
			keyStore.addKey(key);

            Toast.makeText(ImportKeyActivity.this, "Key is Imported.", Toast.LENGTH_LONG).show();
            ImportKeyActivity.this.finish();
		}
	}

	// Called when a QR is decoded
	// "text" : the text encoded in QR
	// "points" : points where QR control points are placed in View
	@Override
	public void onQRCodeRead(String text, PointF[] points)
	{
		keyText = text;
		Utils.println(keyText);
		if (!found)
		{
			found = true;
			showPasswordDialog();
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		qrCodeReaderView.startCamera();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		qrCodeReaderView.stopCamera();
	}

	public void showPasswordDialog()
	{
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.reveal_key_dialog);
		dialog.setCancelable(true);

		Button unlock = (Button) dialog.findViewById(R.id.unlock);
		unlock.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();

				Aes256Encryptor aes256Encryptor = new Aes256Encryptor(new Utils().generateMd5(((EditText) dialog.findViewById(R.id.password)).getText().toString()));
				plainKey = aes256Encryptor.decrypt(keyText);

				if (plainKey != null)
				{
					Toast.makeText(ImportKeyActivity.this, "Key Unlocked.", Toast.LENGTH_LONG).show();
				}
				else
				{
					found = false;
					Toast.makeText(ImportKeyActivity.this, "Password error.", Toast.LENGTH_LONG).show();
				}
			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
