package com.SCVA.KeyShare.models;

import java.util.Date;

import com.SCVA.KeyShare.Utils.Utils;
import com.SCVA.encryption.Aes256Encryptor;

import android.os.Parcel;
import android.os.Parcelable;

public class Key implements Parcelable
{
	private int id;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	private String name;
	private String keyText;
	private boolean isDefault;
	private Date createdOn;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getKeyText()
	{
		return keyText;
	}

	public void setKeyText(String keyText)
	{
		this.keyText = keyText;
	}

	public boolean isDefault()
	{
		return isDefault;
	}

	public void setDefault(boolean aDefault)
	{
		isDefault = aDefault;
	}

	public Date getCreatedOn()
	{
		return createdOn;
	}

	public void setCreatedOn(Date createdOn)
	{
		this.createdOn = createdOn;
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.id);
		dest.writeString(this.name);
		dest.writeString(this.keyText);
		dest.writeByte(this.isDefault ? (byte) 1 : (byte) 0);
		dest.writeLong(this.createdOn != null ? this.createdOn.getTime() : -1);
	}

	public void readFromParcel(Parcel source)
	{
		this.id = source.readInt();
		this.name = source.readString();
		this.keyText = source.readString();
		this.isDefault = source.readByte() != 0;
		long tmpCreatedOn = source.readLong();
		this.createdOn = tmpCreatedOn == -1 ? null : new Date(tmpCreatedOn);
	}

	public Key()
	{
	}

	protected Key(Parcel in)
	{
		this.id = in.readInt();
		this.name = in.readString();
		this.keyText = in.readString();
		this.isDefault = in.readByte() != 0;
		long tmpCreatedOn = in.readLong();
		this.createdOn = tmpCreatedOn == -1 ? null : new Date(tmpCreatedOn);
	}

	public static final Parcelable.Creator<Key> CREATOR = new Parcelable.Creator<Key>()
	{
		@Override
		public Key createFromParcel(Parcel source)
		{
			return new Key(source);
		}

		@Override
		public Key[] newArray(int size)
		{
			return new Key[size];
		}
	};

	public String getPlainKey(String password)
	{
		Aes256Encryptor aes256Encryptor = new Aes256Encryptor(new Utils().generateMd5(password));
		return aes256Encryptor.decrypt(keyText);
	}
}
