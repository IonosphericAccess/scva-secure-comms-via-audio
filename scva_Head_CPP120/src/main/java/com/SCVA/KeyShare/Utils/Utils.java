package com.SCVA.KeyShare.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;

import com.SCVA.R;
import com.SCVA.KeyShare.models.Key;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class Utils
{
	private String chanId = "0";
	 public static boolean log = false;
	private AlertDialog.Builder alrtdialog;
	public NotificationManager mNotificationManager;
	public static Notification myNotification = null;
	public String NOTIFICATION_CHANNEL_ID = "com.scva";
	public static String DATE_FORMAT = "dd-MM-yyyy hh:mm";
	SimpleDateFormat sdf = new SimpleDateFormat(Utils.DATE_FORMAT);

	public static final String APP_DIRECTORY_ROOT = "SCVA";
	private static File sdCardRoot = Environment.getExternalStorageDirectory();// SCVA.myContext.getDataDir();//ExternalFilesDir(Environment.DIRECTORY_PICTURES);

	public void writeToFile(String data, Context context)
	{
		try
		{
			File yourDir = new File(sdCardRoot, APP_DIRECTORY_ROOT + "/");

			File[] fileList = yourDir.listFiles();
			if (yourDir != null && fileList != null)
			{
			}
			else
			{
				yourDir.mkdirs();
			}
			File file = new File(yourDir, "msgs.log");
			FileOutputStream stream = new FileOutputStream(file, true);
			try
			{
				stream.write(new String(sdf.format(System.currentTimeMillis()) + " | " + data + "\n").getBytes());
			}
			finally
			{
				stream.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	public String readLogFile(Context context)
	{
		StringBuilder text = new StringBuilder();
		File yourDir = new File(sdCardRoot, APP_DIRECTORY_ROOT + "/");

		File[] fileList = yourDir.listFiles();
		if (yourDir != null && fileList != null)
		{
		}
		else
		{
			yourDir.mkdirs();
		}
		File file = new File(yourDir, "msgs.log");
		if (file.exists())
		{
			try
			{
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null)
				{
					text.append(line);
					text.append('\n');
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		if (text.toString().length() == 0)
		{
			text.append("Nothing to show here...");
		}
		return text.toString();
	}

	public void burnLogs(Context context)
	{
		try
		{
			File yourDir = new File(sdCardRoot, APP_DIRECTORY_ROOT + "/");
			File[] fileList = yourDir.listFiles();
			if (yourDir != null && fileList != null)
			{
			}
			else
			{
				yourDir.mkdirs();
			}
			File file = new File(yourDir, "msgs.log");
			if (file.exists()) file.delete();
			if (yourDir.exists()) yourDir.delete();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	public static void println(Object obj)
	{
		if (log) System.out.println(obj);
	}

	public String generateMd5(String username)
	{
		try
		{
			final MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(username.getBytes());
			final byte[] bytes = digest.digest();
			final StringBuffer buffer = new StringBuffer();

			for (int i = 0; i < bytes.length; i++)
			{
				String hex = Integer.toHexString(0xFF & bytes[i]);
				if (hex.length() == 1)
				{
					hex = "0" + hex;
				}
				buffer.append(hex);
			}
			return buffer.toString();
		}
		catch (Exception e)
		{
			return "";
		}
	}

	public void showNotification(Context context, KeyStore keyStore)
	{
		String keyName = "No key is Unlocked";
		Key key = keyStore.getDefaultKey();
		if (key != null)
		{
			keyName = "Active key is " + key.getName();
		}
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		int notificationId = 0;
		String channelId = "KeyManager";
		String channelName = "Key Manager";
		int importance = NotificationManager.IMPORTANCE_HIGH;

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
		{
			NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
			notificationManager.createNotificationChannel(mChannel);
		}

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId).setSmallIcon(R.drawable.ic_key)
				.setContentTitle(context.getString(R.string.txt_app_name)).setContentText(keyName).setOngoing(true);
		// TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

		// stackBuilder.addNextIntent(intent);
		// PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
		// PendingIntent.FLAG_UPDATE_CURRENT);
		// mBuilder.setContentIntent(resultPendingIntent);

		notificationManager.notify(notificationId, mBuilder.build());
	}

	public void hideNotification(Context context)
	{
		mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(0);
	}

	public Runnable showQuestionDialog(final String title, final String message, final Context context, final Runnable yes, final Runnable no)
	{
		Runnable populate = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					final Handler handler = new Handler();
					alrtdialog = new AlertDialog.Builder(context);
					alrtdialog.setTitle(title);
					alrtdialog.setMessage(message);
					alrtdialog.setCancelable(true).setNeutralButton("Yes", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int id)
						{
							if (yes != null) handler.post(yes);
							dialog.dismiss();
							dialog = null;
						}
					});
					alrtdialog.setCancelable(true).setPositiveButton("No", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int id)
						{
							if (no != null) handler.post(no);
							dialog.dismiss();
							dialog = null;
						}
					});
					alrtdialog.show();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		};
		return populate;
	}
}
