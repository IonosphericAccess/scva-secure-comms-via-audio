package com.SCVA.KeyShare.Utils;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.SCVA.SCVA;
import com.SCVA.KeyShare.models.Key;

public class KeyStore
{
	public ArrayList<Key> readKeys()
	{
		ArrayList<Key> tmp = new ArrayList<>();
		String keysText = SCVA.mysp.getString("enc_keys", "");
		// Utils.println("KEYS TEXT =|= " + keysText);
		if (keysText.length() > 0)
		{
			try
			{
				JSONArray keys = new JSONArray(keysText);
				for (int i = 0; i < keys.length(); i++)
				{
					JSONObject jKey = (JSONObject) keys.get(i);

					Key tmpKey = new Key();

					tmpKey.setId(jKey.getInt("id"));
					tmpKey.setName(jKey.getString("name"));
					tmpKey.setKeyText(jKey.getString("keyText"));
					tmpKey.setCreatedOn(new Date(jKey.getLong("date")));
					tmpKey.setDefault(jKey.getBoolean("default"));

					tmp.add(tmpKey);
				}
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return tmp;
	}

	public void addKey(Key key)
	{
		ArrayList<Key> tmpKeys = readKeys();
		key.setId(tmpKeys.size() + 1);
		tmpKeys.add(key);
		saveKeys(tmpKeys);

		// SCVA.mysp.edit().putString("enc_key",kk).commit();
	}

	public void deleteKey(Key key)
	{
		ArrayList<Key> tmpKeys = readKeys();
		for (int i = 0; i < tmpKeys.size(); i++)
		{
			if (tmpKeys.get(i).getId() == key.getId())
			{
				if (tmpKeys.get(i).isDefault())
				{
					SCVA.mysp.edit().putString("enc_key", "").commit();
				}
				tmpKeys.remove(i);
			}
		}
		saveKeys(tmpKeys);
	}

	public void markKeyAsDefault(Key key)
	{
		ArrayList<Key> tmpKeys = readKeys();
		for (int i = 0; i < tmpKeys.size(); i++)
		{
			tmpKeys.get(i).setDefault(false);
		}
		for (int i = 0; i < tmpKeys.size(); i++)
		{
			if (tmpKeys.get(i).getId() == key.getId())
			{
				tmpKeys.get(i).setDefault(key.isDefault());
				break;
			}
		}
		saveKeys(tmpKeys);
	}

	public Key getDefaultKey()
	{
		ArrayList<Key> tmpKeys = readKeys();
		for (int i = 0; i < tmpKeys.size(); i++)
		{
			if (tmpKeys.get(i).isDefault())
			{
				return tmpKeys.get(i);
			}
		}
		return null;
	}

	private void saveKeys(ArrayList<Key> tmpKeys)
	{
		JSONArray jKeys = new JSONArray();
		for (int i = 0; i < tmpKeys.size(); i++)
		{
			try
			{
				Key key = tmpKeys.get(i);
				JSONObject jKey = new JSONObject();
				jKey.put("id", key.getId());
				jKey.put("name", key.getName());
				jKey.put("keyText", key.getKeyText());
				jKey.put("date", key.getCreatedOn().getTime());
				jKey.put("default", key.isDefault());

				jKeys.put(jKey);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		// Utils.println("ADD KEYS =|= " + jKeys.toString());
		SCVA.mysp.edit().putString("enc_keys", jKeys.toString()).commit();
	}

	public void burnKeyStore()
	{
		SCVA.mysp.edit().putString("enc_keys", "").commit();
	}
}
