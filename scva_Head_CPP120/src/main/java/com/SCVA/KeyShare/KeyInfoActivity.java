package com.SCVA.KeyShare;

import java.text.SimpleDateFormat;

import com.SCVA.R;
import com.SCVA.KeyShare.Utils.Contents;
import com.SCVA.KeyShare.Utils.KeyStore;
import com.SCVA.KeyShare.Utils.QRCodeEncoder;
import com.SCVA.KeyShare.Utils.Utils;
import com.SCVA.KeyShare.models.Key;
import com.google.zxing.BarcodeFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class KeyInfoActivity extends Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener
{
	private Key key;
	private TextView name, date;
	private Button save;
	private ImageView qrCode;
	private CheckBox isDefault;
	private KeyStore keyStore;
	private Bitmap qrCodeImage = null;
	public static SharedPreferences mysp;
	private AlertDialog.Builder alrtdialog;
	private Button reveal, share;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_key_info);

		key = getIntent().getParcelableExtra("KEY");
		mysp = PreferenceManager.getDefaultSharedPreferences(this);

		keyStore = new KeyStore();

		name = findViewById(R.id.name);
		name.setText("" + key.getName());

		date = findViewById(R.id.date);
		SimpleDateFormat sdf = new SimpleDateFormat(Utils.DATE_FORMAT);
		date.setText("" + sdf.format(key.getCreatedOn()));

		qrCode = findViewById(R.id.qrCode);
		qrCodeImage = getQrCode(key.getKeyText(), 1);
		qrCode.setImageBitmap(qrCodeImage);

		isDefault = findViewById(R.id.isDefault);
		isDefault.setChecked(key.isDefault());
		isDefault.setOnCheckedChangeListener(this);

		save = findViewById(R.id.save);
		save.setOnClickListener(this);

		reveal = findViewById(R.id.reveal);
		reveal.setOnClickListener(this);

		share = findViewById(R.id.share);
		share.setOnClickListener(this);
	}

	@Override
	public void onClick(View view)
	{
		if (view == save)
		{
			Utils.println("Delete Key");
			runOnUiThread(showWarningDialog());
		}
		if (view == reveal)
		{
			showPasswordDialog();
		}
		if (view == share)
		{
			String path = MediaStore.Images.Media.insertImage(getContentResolver(), getQrCode(key.getKeyText(), 180), "Image Description", null);
			Uri uri = Uri.parse(path);

			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("image/jpeg");
			intent.putExtra(Intent.EXTRA_STREAM, uri);
			startActivity(Intent.createChooser(intent, "Share Key"));
		}
	}

	public Bitmap getQrCode(String key, int dimension)
	{
		try
		{
			QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(key, null, Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), dimension);
			return qrCodeEncoder.encodeAsBitmap();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		key.setDefault(isChecked);
		keyStore.markKeyAsDefault(key);
	}

	public Runnable showWarningDialog()
	{
		Runnable populate = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					final Handler handler = new Handler();
					alrtdialog = new AlertDialog.Builder(KeyInfoActivity.this);
					alrtdialog.setTitle("Warning");
					alrtdialog.setMessage("Are you sure you want to delete this key");
					alrtdialog.setCancelable(true).setNegativeButton("YES", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int id)
						{
							keyStore.deleteKey(key);
							dialog.dismiss();
							dialog = null;
                            new Utils().showNotification(KeyInfoActivity.this, keyStore);
							KeyInfoActivity.this.finish();
						}
					});
					alrtdialog.setCancelable(true).setPositiveButton("NO", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int id)
						{
							dialog.dismiss();
							dialog = null;
						}
					});
					alrtdialog.show();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		};
		return populate;
	}

	public void showPasswordDialog()
	{
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.reveal_key_dialog);
		dialog.setCancelable(true);

		Button unlock = (Button) dialog.findViewById(R.id.unlock);
		unlock.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				String plainKey = key.getPlainKey(((EditText) dialog.findViewById(R.id.password)).getText().toString());
				if (plainKey != null)
				{
					KeysActivity.mysp.edit().putString("enc_key", plainKey).commit();
					key.setDefault(true);
					keyStore.markKeyAsDefault(key);

					qrCodeImage = getQrCode(key.getKeyText(), 180);
					qrCode.setImageBitmap(qrCodeImage);
                    isDefault.setChecked(true);
                    reveal.setVisibility(View.GONE);
                    share.setVisibility(View.VISIBLE);
					new Utils().showNotification(KeyInfoActivity.this, keyStore);
					Toast.makeText(KeyInfoActivity.this, "Key is unlocked.", Toast.LENGTH_LONG).show();
				}
				else
					Toast.makeText(KeyInfoActivity.this, "Password error.", Toast.LENGTH_LONG).show();
			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}